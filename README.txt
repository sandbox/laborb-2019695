
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Christoph Heger <heger@laborb.de>

Crontab Manager is a Drupal plugin to manage your crontab file entries.
If you don't want to use the command line to create, edit or delete cron jobs
in the crontab file, this Module enables you to edit it in a drupal-form. 
You can enable site-administrators to edit the cron jobs, without giving 
them command-line access to your server.

INSTALLATION
------------

You only have to enable the module, there will be a new menu item to access
the functionality.

REQUIREMENTS
------------
The Webserver-user needs the permission to edit his crontab file.


RISKS OF USE
------------

Of course, cron jobs are a security issue, so it is highly recommend to use 
this module only in a secure environment. You have to ensure, that nobody 
without permission can access this module, because cron jobs can affect
your system directly.