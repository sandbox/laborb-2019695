<?php

/**
 * @file
 * File with classes for crontab_manager
 *
 * @author Christoph Heger
 */

/**
 * Class to read and write crontab files.
 */
class CrontabManager {

  protected $crontabArr = array();

  /**
   * Implements Constructer, starts parse function.
   */
  public function __construct() {
    $this->parse();
  }

  /**
   * Function parses the crontab file and stores it as an array of arrays.
   */
  protected function parse() {
    $this->crontabArr = array();
    $rows_arr = array();

    exec("crontab -l", $rows_arr);

    foreach ($rows_arr as $row_string) {
      $job_arr_tmp = explode(" ", trim($row_string), 6);
      $job_arr = array(
        'minute' => $job_arr_tmp[0],
        'hour' => $job_arr_tmp[1],
        'day_of_month' => $job_arr_tmp[2],
        'month' => $job_arr_tmp[3],
        'day_of_week' => $job_arr_tmp[4],
        'command' => $job_arr_tmp[5],
      );
      array_push($this->crontabArr, $job_arr);
    }

  }

  /**
   * Returns an array with all current cronjobs as arrays.
   *
   * @return Array
   *   array of arrays with the current cronjobs
   */
  public function getCronjobs() {
    return $this->crontabArr;
  }

  /**
   * Returns an array with a single cronjob.
   *
   * @return array
   *   of cronjob with $id.
   */
  public function getCronjob($id) {
    return $this->crontabArr[$id];
  }

  /**
   * Adds a new job to the crontab array.
   *
   * @param Array $job_arr
   *   array with 6 elements (minute, hour, day of month, month,
   *   day of week, command)
   *
   * @return boolean
   *   TRUE if the job was well formed an added to array, FALSE
   *   if the job wasn't well formed
   */
  public function add($job_arr = array()) {
    if (count($job_arr) != 6) {
      $return_boolean = FALSE;
    }
    else {
      array_push($this->crontabArr, $job_arr);
      if ($this->save()) {
        $return_boolean = TRUE;
      }
      else {
        $return_boolean = FALSE;
      }
    }

    return $return_boolean;
  }

  /**
   * Removes Cronjob with given ID. The ID is the position in the cronjob file.
   */
  public function remove($job_id) {
    unset($this->crontabArr[$job_id]);
    if ($this->save()) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Edits Cronjob with given ID. The ID is the position in the cronjob file.
   *
   * @param int $job_id
   *   Id of the job to edit
   * @param Array $job_arr
   *   Array with job values.
   */
  public function edit($job_id, $job_arr) {
    if (count($job_arr) != 6) {
      $return_boolean = FALSE;
    }
    else {
      $this->crontabArr[$job_id] = $job_arr;
      if ($this->save()) {
        $return_boolean = TRUE;
      }
      else {
        $return_boolean = FALSE;
      }
    }

    return $return_boolean;
  }

  /**
   * Saves the crontab file.
   */
  protected function save() {
    $return_boolean = TRUE;
    try {
      $file = tempnam(sys_get_temp_dir(), "crontab");
      $handle = fopen($file, 'w');
      // Writes all cronjobs in the temporary file.
      foreach ($this->crontabArr as $job_arr) {
        fwrite($handle, implode(" ", $job_arr) . "\n");
      }

      fclose($handle);

      // Executes crontab with the path of the temp file, then deletes the file.
      exec("crontab $file");
      unlink($file);
    }
    catch (Exception $e) {
      echo "Exception" . $e->getMessage();
      $return_boolean = FALSE;
    }
    // Refresh cronjob array.
    $this->parse();
    return $return_boolean;
  }
}
