<?php

/**
 * @file
 * config file for crontab_manager,  $preset_options stores
 * different preset names,
 * $preset_content stores the data of the different presets.
 */

$preset_options = array(
  '' => "Presets",
  '5m' => "Every 5 Minutes",
  '10m' => "Every 10 Minutes",
);

$preset_content = array(
  '5m' => array(
    'minute' => '*/5',
    'hour' => '*',
    'day_of_month' => '*',
    'month' => '*',
    'day_of_week' => '*',
  ),
  '10m' => array(
    'minute' => '*/10',
    'hour' => '*',
    'day_of_month' => '*',
    'month' => '*',
    'day_of_week' => '*',
  ),
);
